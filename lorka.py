import sys
import numpy as np
import pandas as pd
from scipy.spatial import distance
from Bio.PDB import PDBParser,MMCIFParser
import os
import gzip
import tempfile

def build_pdb_path(pdbid,keep=False):
    fname = "{}.cif.gz".format(pdb_id)
    dname = pdb_id[1:3]
    return PERM_DIR if keep else TMP_DIR,dname,fname

def get_pdb_file(pdb_id,keep):
    pdb_path_parts = build_pdb_path(pdb_id,keep)
    pdb_grp = pdb_path_parts[1]
    pdb_dir  = "/".join(pdb_path_parts[0:2])
    pdb_path = "/".join(pdb_path_parts)
    pdb_fname = pdb_path_parts[2]

    if not os.path.isfile(pdb_path):
        from ftplib import FTP

        ftp = FTP("ftp.wwpdb.org")
        ftp.login("anonymous", "")
        ftp.cwd(f"/pub/pdb/data/structures/divided/mmCIF/{pdb_grp}")

        if not os.path.exists(pdb_dir):
            os.makedirs(pdb_dir)

        try:
            with open(pdb_path,"wb") as ofh:
                ftp.retrbinary("RETR " + pdb_fname ,ofh.write)
        except:
            raise Exception("Could not download pdb file for {}".format(pdb_id))

        ftp.close()

#######################################################################################################
#
# Parameters
#
#######################################################################################################

k_b = 0.0019872041 # kcal/(mol*K)
T = 300 # K
l = 10.0 # Angstroms

Kelec = 4.85

alpha = [0.1, 0.1, 0.1] # [Polar, Non-Polar, Ionizable]
r_max = [5.0, 7.0, 5.0] # [Polar, Non-Polar, Ionizable]
Ap = [0.749, 0.429, 0.112,  0.096] # [Asp, Glu, Lys-Arg, His]
Anp = [4.72, 4.2, 5.43, 4.87] # [Asp, Glu, Lys-Arg, His]
pka_wat = [4.0, 4.5, 10.6, 12.0, 6.4, 8.0, 3.6] # [Asp, Glu, Lys, Arg, His, N-ter, C-ter]

NeighMax = [4.580, 12.741] # [Polar, Non-Polar]
slope = [0.1, 2.46] # [Polar, Non-Polar]


#######################################################################################################
#
# Functions
#
#######################################################################################################

##
# List: Three letter code for aminoacids
##

res_list_three = ['GLY', 'ALA', 'VAL', 'LEU', 'MET',
                  'ILE', 'SER', 'THR', 'CYS', 'ASN',
                  'GLN', 'LYS', 'ARG', 'ASP', 'GLU',
                  'HIS', 'PHE', 'TYR', 'TRP', 'PRO']

##
# Aminoacids called "rings", for use in ring true positions calculations
# Three letter code is used. Also atoms in each ring and a dict to easy access.
##

ring_list_three = ['HIS', 'PHE', 'TYR', 'TRP']
HIS_rg_atoms = ['CG', 'ND1', 'CD2', 'CE1', 'NE2']
PHE_rg_atoms = ['CG', 'CD1', 'CD2', 'CE1', 'CE2', 'CZ']
TYR_rg_atoms = ['CG', 'CD1', 'CD2', 'CE1', 'CE2', 'CZ', 'OH']
TRP_rg_atoms = ['CG', 'CD1', 'CD2', 'NE1', 'CE2', 'CE3', 'CZ2', 'CZ3', 'CH2']
rg_dict = {'HIS': HIS_rg_atoms,
           'PHE' : PHE_rg_atoms,
           'TYR' : TYR_rg_atoms,
           'TRP' : TRP_rg_atoms}

##
# Dictionary: Translate from three letter code to one letter code
##

three_to_one = {'ALA' : 'A', 'ARG' : 'R', 'ASN' : 'N', 'ASP' : 'D',
                'CYS' : 'C', 'GLU' : 'E', 'GLN' : 'Q', 'GLY' : 'G',
                'HIS' : 'H', 'ILE' : 'I', 'LEU' : 'L', 'LYS' : 'K',
                'MET' : 'M', 'PHE' : 'F', 'PRO' : 'P', 'SER' : 'S',
                'THR' : 'T', 'TRP' : 'T', 'TYR' : 'Y', 'VAL' : 'V'}

##
# Dictionary: Translate from three letter code to one letter type code of aminoacids
#
# Types: N -> Non Polar
#        P -> Polar
#        A -> Acid
#        B -> Base
#        G -> Glycine
##

type_dict = {'ALA' : 'N', 'ARG' : 'B', 'ASN' : 'P', 'ASP' : 'A', 'CYS' : 'P',
             'GLU' : 'A', 'GLN' : 'P', 'GLY' : 'G', 'HIS' : 'B', 'ILE' : 'N',
             'LEU' : 'N', 'LYS' : 'B', 'MET' : 'N', 'PHE' : 'N', 'PRO' : 'N',
             'SER' : 'P', 'THR' : 'P', 'TRP' : 'N', 'TYR' : 'P', 'VAL' : 'N'}

##
# Dictionary: Reference pKas in water for three letter code
##

ref_pkas = {'ASP' : 4.0, 'GLU' : 4.5, 'LYS' : 10.6, 'ARG' : 12.0, 'HIS' : 6.4}

##
# Dictionary: Reference pKas in water for one letter code
##

ref_pkas_one = {'D' : 4.0, 'E' : 4.5, 'K' : 10.6, 'R' : 12.0, 'H' : 6.4}

##
# Dictionary: Charged residues type in one letter code
#
# Types: A -> Acid
#        B -> Base
##

charged_classification = {'D': 'A', 'E': 'A', 'K': 'B',
                          'R': 'B', 'H': 'B', 'X': 'B', 'Z': 'A'}

##
# Three letter to one aminoacid code converter
##

def three_code_one(three_key):
    val = three_to_one[three_key]
    return val

##
# One letter charged aa to type
# Acid: A or Base: B result 
##

def charged_to_type(one_key):
    val = charged_classification[one_key]
    return val

##
# Function to assign reference pKas that are listed
# in the dictionary ref_pkas
##

def reference_pka(three_key):
    val = ref_pkas[three_key]
    return val

##
# Function to assign reference pKas that are listed
# in the dictionary ref_pkas_one FOR ONE LETTER CODE
##

def one_reference_pka(one_key):
    val = ref_pkas_one[one_key]
    return val

##
# Function to clasify aminoacids in types
# Types are: Acid, Base, Polar, NonPolar, Glycine
# Code type: A, B, P, N, G
# The input is the aminoacid in three letter code
# The output is the code letter.
##

def type_aa(three_key):
    val = type_dict[three_key]
    return val

##
# Function to clean a list of elements from a PDB and get only aminoacids.
# It cleans the PDB from other molecules that are not aminoacids. 
# Recieves the full list of residues as input and returns a list only with
# aminoacids. It uses the list defined above called res_list_three.
##

def only_residues(full_list):
    output_res = []
    for r in full_list:
        r_name = r.get_resname()
        if r_name in res_list_three:
            output_res.append(r)
    return output_res

##
# Functions to calculate a geometric "true position" for aminoacid with rings.
# Receives the residue and based on identity returns a "true position" (array tp)
# for the ring.
##

def ring_position(ring_residue):
    rg_name = ring_residue.get_resname()
    if rg_name not in ring_list_three:
        raise ValueError('The residue is not in rings list')
    at_in_rg = rg_dict[rg_name]
    # List of relevant atoms for the ring
    atom_values = []
    for a in ring_residue:
        atom_name = a.get_name()
        if atom_name in at_in_rg:
            atom_values.append(a)
    # Geometric center calculation
    norm = len(atom_values)
    x_mean = 0.0
    y_mean = 0.0
    z_mean = 0.0

    for a in atom_values:
        pos = a.get_vector()
        x_mean += pos[0]
        y_mean += pos[1]
        z_mean += pos[2]


    x_mean = x_mean/norm
    y_mean = y_mean/norm
    z_mean = z_mean/norm

    true_pos = [x_mean, y_mean, z_mean]

    return true_pos

##
# Function to clean PDB and get important data
# for ca cb atoms.
# - Input: pdb id, struct num,
# - Output: ca posit, rep posit, letters, types
#           As dicionary with keys: ca, cb, let, typ
##

def clean_pdb(pdb_id, pdb_fname, struct_num, repre, keep):

    # Calculate for ca and side chain representation
    # Representations availiable = ['ca', 'cb', 'long']
    representations = ['ca', repre]

    # Read PDB files
    if pdb_id:
        pdb_path_parts = build_pdb_path(pdb_id,keep)
        dname = pdb_path_parts[1]
        pdb_fname = "/".join(pdb_path_parts)
        get_pdb_file(pdb_id,keep)

    if ".cif" in pdb_fname.lower():
        parser = MMCIFParser()
    else:
        parser = PDBParser()

    if pdb_fname.endswith("gz"):
        with gzip.open(pdb_fname, 'rt') as fh:
            structure = parser.get_structure('-',fh)
    else:
        with open(pdb_fname, 'rt') as fh:
            structure = parser.get_structure('-',fh)
    all_chains = structure[struct_num]
    for chain in all_chains:
        model = chain
        break
    # List of residues and other molecules
    full_list = model.get_residues()
    # Get only a list of residues
    only_r = only_residues(full_list)

    
    # Positions for residue (side chain) and backbone
    # pos = [(res1_x, res1_y, res1_z), ... , (... , resN_y, resN_z)]
    resi_pos = []
    back_pos = []

    # Array of numeration from pdb
    real_num = []

    # Letters and types of residues arrays
    lett_res = []
    type_res = []

    for rep in representations:
        if rep == 'ca':
            info_dict = {'VAL': 'CA', 'TRP': 'CA', 'ILE': 'CA', 'MET': 'CA',
                         'HIS': 'CA', 'TYR': 'CA', 'GLN': 'CA', 'THR': 'CA',
                         'LEU': 'CA', 'ASN': 'CA', 'GLY': 'CA', 'PHE': 'CA',
                         'SER': 'CA', 'ASP': 'CA', 'ALA': 'CA', 'ARG': 'CA',
                         'GLU': 'CA', 'LYS': 'CA', 'CYS': 'CA', 'PRO': 'CA'}
        elif rep == 'cb':
            info_dict = {'VAL': 'CB', 'TRP': 'CB', 'ILE': 'CB', 'MET': 'CB',
                         'HIS': 'CB', 'TYR': 'CB', 'GLN': 'CB', 'THR': 'CB',
                         'LEU': 'CB', 'ASN': 'CB', 'GLY': 'CA', 'PHE': 'CB',
                         'SER': 'CB', 'ASP': 'CB', 'ALA': 'CB', 'ARG': 'CB',
                         'GLU': 'CB', 'LYS': 'CB', 'CYS': 'CB', 'PRO': 'CB'}

        # Write position of residues and information
        for rdx, r in enumerate(only_r):
            name = r.get_resname()
            id_res = r.get_id()
            repres = info_dict[name]
            one_let = three_code_one(name)
            res_type = type_aa(name)
            key = id_res[1]
            if repres == 'RG':
                # The residue was labelled as ring.
                # The residue is placed at the ring's mean position
                # Ring mean position
                tp = ring_position(r)
                if rep == 'ca':
                    back_pos.append([tp[0],tp[1],tp[2]])
                    real_num.append(key)
                    lett_res.append(one_let)
                    type_res.append(res_type)
                else:
                    resi_pos.append([tp[0],tp[1],tp[2]])
            else:
                # Other residues and representations
                # Atom position, try representation,
                # if there is a problem place it in CB
                try:
                    atom_res = r[repres]
                except KeyError:
                    atom_res = r['CB']
                tp = atom_res.get_vector()
                if rep == 'ca':
                    back_pos.append([tp[0],tp[1],tp[2]])
                    real_num.append(key)
                    lett_res.append(one_let)
                    type_res.append(res_type)
                else:
                    resi_pos.append([tp[0],tp[1],tp[2]])

    # Terminals position: N-ter is called 'X', C-ter is callez 'Z'

    # N-ter
    r = only_r[0]
    name = r.get_resname()
    id_res = r.get_id()
    repres = info_dict[name]
    one_let = three_code_one(name)
    res_type = type_aa(name)
    key = id_res[1]
    atom_res = r['N']
    tp = atom_res.get_vector()
    real_num.append(len(only_r) + 1)
    lett_res.append('X')
    type_res.append('X')
    back_pos.append([tp[0],tp[1],tp[2]])
    resi_pos.append([tp[0],tp[1],tp[2]])

    # C-ter
    r = only_r[-1]
    name = r.get_resname()
    id_res = r.get_id()
    repres = info_dict[name]
    one_let = three_code_one(name)
    res_type = type_aa(name)
    key = id_res[1]
    atom_res = r['C']
    tp = atom_res.get_vector()
    real_num.append(len(only_r) + 2)
    lett_res.append('Z')
    type_res.append('Z')
    back_pos.append([tp[0],tp[1],tp[2]])
    resi_pos.append([tp[0],tp[1],tp[2]])
    
    # Output as dictionary
    out = {}
    out['nr'] = real_num
    out['ca'] = back_pos
    out['pos'] = resi_pos
    out['let'] = lett_res
    out['typ'] = type_res
    return out

def electro_from_matrix(dataset,l,charged_indexes,charges):
    # Charged Submatrix
    charged_submatrix = dataset.loc[charged_indexes,charged_indexes]
    electro_field = np.exp(-charged_submatrix/l)/charged_submatrix
    electro_charged = electro_field.mul(charges, axis = 1)
    electro_penalty = electro_charged.replace([np.inf, -np.inf], 0)
    elec = pd.DataFrame({'Elec': electro_penalty.sum(axis = 1)})

    return elec

def neighbours_from_matrix(matrix_dataset,position_dataset,charged_indexes,alpha,r_max):

    polar_indexes = position_dataset.loc[position_dataset['Type'] == 'P'].index
    non_polar_indexes = position_dataset.loc[position_dataset['Type'] == 'N'].index
    
    # Charged Submatrix
    charged_polar_submatrix = matrix_dataset.loc[charged_indexes,
                                                 polar_indexes]
    
    charged_non_polar_submatrix = matrix_dataset.loc[charged_indexes,
                                                     non_polar_indexes]
    
    charged_charged_submatrix = matrix_dataset.loc[charged_indexes,
                                                   charged_indexes]

    # Neighbour Counting
    charged_polar = charged_polar_submatrix.applymap(
                    lambda x: 1.0 if x <= r_max[0]
                    else np.exp(-alpha[0]*(x-r_max[0])*(x-r_max[0])))

    charged_non_polar = charged_non_polar_submatrix.applymap(
                        lambda x: 1.0 if x <= r_max[1]
                        else np.exp(-alpha[1]*(x-r_max[1])*(x-r_max[1])))
    
    charged_charged = charged_charged_submatrix.applymap(
                      lambda x: 0.0 if x == 0.0
                      else (1.0 if x <= r_max[2]
                      else np.exp(-alpha[2]*(x-r_max[2])*(x-r_max[2]))))

    # Total Neighbour Number
    polar = charged_polar.sum(axis = 1)
    non_polar = charged_non_polar.sum(axis = 1)
    ionizable = charged_charged.sum(axis = 1)
    
    dataset = pd.DataFrame({'Np': polar , 'Nnp': non_polar,
                            'Ni': ionizable, 'Npi': polar + ionizable,
                            'Let': position_dataset.loc[charged_indexes,'Let']})

    return dataset

def polarity_from_neighbours(neighbours, slope, NeighMax, Ap, Anp):
    # Calculation
    p_pen = neighbours['Npi'].apply(lambda x: 1.0 if x >= NeighMax[0]
                                    else np.exp(-slope[0]*
                                                (x-NeighMax[0])*
                                                (x-NeighMax[0])))
    
    n_p_pen = neighbours['Nnp'].apply(lambda x: 1.0 if x >= NeighMax[1]
                                      else np.exp(-slope[1]*
                                                  (x-NeighMax[1])*
                                                  (x-NeighMax[1])))

    # Output DataFrame
    self_penalty = pd.DataFrame({'Polar': p_pen, 'Non-Polar': n_p_pen,
                                 'Let': neighbours['Let']})

    # Signs for acids and bases 
    self_penalty.loc[(self_penalty['Let'] == 'R') |
                     (self_penalty['Let'] == 'K') |
                     (self_penalty['Let'] == 'H') |
                     (self_penalty['Let'] == 'X'),'Non-Polar'] *= -1
    
    self_penalty.loc[(self_penalty['Let'] == 'E') |
                     (self_penalty['Let'] == 'D') |
                     (self_penalty['Let'] == 'Z'),'Polar'] *= -1

    return self_penalty

def parsearguments():
    import argparse,sys

    epilog = """============================== Alea jacta est =============================="""
    parser = argparse.ArgumentParser(epilog=epilog,formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    group_pdb = parser.add_mutually_exclusive_group(required=True)
    group_pdb.add_argument('--pdbid', help='A PDB ID to process (will be automatically downloaded)')
    group_pdb.add_argument('--pdbfile', help='A local PDB file to process')

    parser.add_argument('--keep', help='Keep a local copy of the PDB file', action="store_true")

    if len(sys.argv)==1:
        parser.print_help()
        sys.exit(1)

    return(vars(parser.parse_args()))

def main(pdb_id,pdb_fname,keep):
    #######################################################################################################
    #
    # Calculation
    #
    #######################################################################################################
    ##    
    # Structure Number and Chain Name
    ##
    struct_num = 0

    ##
    # Position of side-chain
    ##
    rep = 'cb'

    ##
    # Get Ca Pos Let Type data from PDB
    ##
    out = clean_pdb(pdb_id, pdb_fname, struct_num, rep, keep)

    ##
    # Convert to Pandas DataFrame
    ##
    ca = np.array(out['ca'])
    cax = ca[:,0]
    cay = ca[:,1]
    caz = ca[:,2]

    pos = np.array(out['pos'])
    posx = pos[:,0]
    posy = pos[:,1]
    posz = pos[:,2]

    let = np.array(out['let'])
    typ = np.array(out['typ'])
    nr = np.array(out['nr'])
    nidx = np.arange(1,len(nr)+1)

    dataset = pd.DataFrame({'Nr': nr, 'Nidx': nidx, 'CaX': cax,
                            'CaY': cay, 'CaZ': caz, 'PosX': posx,
                            'PosY': posy, 'PosZ': posz, 'Let': let, 'Type': typ})
    header = ['Nr','Nidx','CaX','CaY','CaZ','PosX','PosY','PosZ','Let','Type']
    dataset = dataset[header]

    ##
    # Matrix of euclidean distances
    ##
    dist_matrix = distance.cdist(pos,pos, 'euclidean')
    n_matrix = np.arange(0,len(nr))
    matrix_dataset = pd.DataFrame(data = dist_matrix, columns = n_matrix)


    # Calculation of pKa
        
    charged = dataset.loc[(dataset['Type'] == 'A') |
                          (dataset['Type'] == 'B') |
                          (dataset['Type'] == 'X') |
                          (dataset['Type'] == 'Z')]
        
    charged_indexes = np.array(charged.index)
        
    # Calculate Electrostatic Contribution
    charges = np.array(charged['Type'].replace(['A','B','X','Z'],[-1,1,1,-1]))
    elec = electro_from_matrix(matrix_dataset,l,charged_indexes,charges)

    # Count Neighbours
    neighbours = neighbours_from_matrix(matrix_dataset,dataset,charged_indexes,alpha,r_max)

    # Calculate Polarity Environment Contribution
    self_penalty = polarity_from_neighbours(neighbours,slope,NeighMax,Ap,Anp)
       
    # Note: Minus in Elec Penalty and Energy normalization with (k_b * T)
    result = pd.DataFrame({'Let': self_penalty['Let'],
                           'Elec': elec['Elec'] * (-1),
                           'Unp': self_penalty['Non-Polar'],
                           'Up': self_penalty['Polar'],
                           'Np': neighbours['Np'],
                           'Nnp': neighbours['Nnp'],
                           'Ni': neighbours['Ni'],
                           'Npi': neighbours['Npi'],
                           'pKaWat': np.zeros(len(elec['Elec'])),
                           'pKaComp': np.zeros(len(elec['Elec'])),
                           'ShiftComp': np.zeros(len(elec['Elec']))})


    #######################################################################################################
    #
    # Strength of interactions
    #
    #######################################################################################################

    # Electrostatic

    result['Elec'] *= Kelec / (k_b * T * np.log(10))

    # Asp, Glu, Lys-Arg, His

    # Polar
    result.loc[(result['Let'] == 'D'), 'Up'] *= Ap[0] / (k_b * T * np.log(10))
    result.loc[(result['Let'] == 'E'), 'Up'] *= Ap[1] / (k_b * T * np.log(10))
    result.loc[(result['Let'] == 'K') |
               (result['Let'] == 'R'), 'Up'] *= Ap[2] / (k_b * T * np.log(10))
    result.loc[(result['Let'] == 'H'), 'Up'] *= Ap[3] / (k_b * T * np.log(10))

    # Non-Polar
    result.loc[(result['Let'] == 'D'), 'Unp'] *= Anp[0] / (k_b * T * np.log(10))
    result.loc[(result['Let'] == 'E'), 'Unp'] *= Anp[1] / (k_b * T * np.log(10))
    result.loc[(result['Let'] == 'K') |
               (result['Let'] == 'R'), 'Unp'] *= Anp[2] / (k_b * T * np.log(10))
    result.loc[(result['Let'] == 'H'), 'Unp'] *= Anp[3] / (k_b * T * np.log(10))

    # pKaWat
    result.loc[(result['Let']=='D'),'pKaWat'] = pka_wat[0]
    result.loc[(result['Let']=='E'),'pKaWat'] = pka_wat[1]
    result.loc[(result['Let']=='K'),'pKaWat'] = pka_wat[2]
    result.loc[(result['Let']=='R'),'pKaWat'] = pka_wat[3]
    result.loc[(result['Let']=='H'),'pKaWat'] = pka_wat[4]
    result.loc[(result['Let']=='X'),'pKaWat'] = pka_wat[5]
    result.loc[(result['Let']=='Z'),'pKaWat'] = pka_wat[6]
                                                        
    # Calculate pKa and Shift
    pka_calc = result['pKaWat'] + result['Elec'] + result['Unp'] + result['Up']
    shift_calc = result['Elec'] + result['Unp'] + result['Up']
    result['pKaComp'] = pka_calc
    result['ShiftComp'] = shift_calc

    print(result.to_string())

if __name__ == "__main__":
    import argparse

    TMP_DIR = "/tmp/lorka"
    PERM_DIR = "pdbs"

    args = parsearguments()

    pdb_id = args["pdbid"]
    if pdb_id:
        pdb_id = pdb_id.lower()
    pdb_fname = args["pdbfile"]

    keep = args["keep"]

    main(pdb_id,pdb_fname,keep)
